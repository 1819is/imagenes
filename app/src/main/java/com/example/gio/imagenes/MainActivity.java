package com.example.gio.imagenes;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView imagen2;
    private Button btnCambiarImagen;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imagen2 = findViewById(R.id.imagen2);
        btnCambiarImagen = findViewById(R.id.btnCambiarImagen);
        btnCambiarImagen.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btnCambiarImagen:
                imagen2.setImageResource(R.drawable.android_3);
                break;
        }
    }
}
